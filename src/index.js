import 'bootstrap';
import 'jquery';
import './scss/styles.scss';
console.log('tick');

$(document).ready(function () {
  //Scroll Animation Actived

  $(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    $('.anim').each(function (i) {
      var $box = $('.anim').eq(i);
      var offset = $box.offset();
      if (scrollTop > offset.top - $(window).height() / 2) {
        $box.addClass('active');
      } else {
        $box.removeClass('active');
      }
    });

    ///Scoll on Flowing
    document.body.style.setProperty(
      '--scroll',
      window.pageYOffset / (document.body.offsetHeight - window.innerHeight)
    );
  });

  // Loader Page Animation
  $('.loader').addClass('active');

  //SEND BUTTON ANIMATON
  $('.send-button').on('click', function () {
    $(this).addClass('send-active');

    setTimeout(function () {
      $('.okey-send-text').css('display', 'block');
    }, 1500);
  });

  // MENU TOGGLE
  $('.toggle').on('click', function () {
    $('.toggle').toggleClass('on-off');
    $('.navbar').toggleClass('navbar-active');
  });
});
